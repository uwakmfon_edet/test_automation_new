/**
 * Sample automated test scenario for Nightwatch.js
 *
 * > it navigates to google.com and searches for nightwatch,
 *   verifying if the term 'The Night Watch' exists in the search results
 */

module.exports = {
  'demo test google' : function (client) {
    client
      .url('http://elearn.abudlc2.test.vggdev.com')
      .setValue('id[UserName]', 'test31mba')
      .setValue('id[Password]', 'password1')
      .useXpath()
     .click('//*[@id="content"]/div/div/div/form/div/button')
     .useCSS()
     .verify.title('Dashboard - Vigilearn')
     .end()
  }
};
