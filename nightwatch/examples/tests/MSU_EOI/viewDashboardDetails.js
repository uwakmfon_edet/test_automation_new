module.exports = {
  'View Dashboard details' : function (browser) {
    browser
   .url('https://register-test.agriyouthlab.com')
     // .waitForElementVisible('body', 6000)
      .useXpath()
      .verify.elementPresent('//*[@id="idIHave"]')
      .click('//*[@id="idIHave"]')
      .pause(1000)
      
      // SSO Login
     //   .waitForElementVisible('body', 60000)
        .verify.urlContains('https://sso-test.agriyouthlab.com/')
        .pause(1000)
        .useXpath()
        .verify.elementPresent('//*[@id="loginform"]/div[1]/div/input') 
        .setValue('//*[@id="loginform"]/div[1]/div/input', 'bukola')
        .verify.elementPresent('//*[@id="loginform"]/div[2]/div/input') 
        .setValue('//*[@id="loginform"]/div[2]/div/input', 'password1')
        .verify.elementPresent('//*[@id="loginform"]/div[4]/div/button') 
        .click('//*[@id="loginform"]/div[4]/div/button')
        // End of SSO Login
      
      .verify.urlEquals('https://register-test.agriyouthlab.com/Admin/Dashboard')
      .end()

  }
}
