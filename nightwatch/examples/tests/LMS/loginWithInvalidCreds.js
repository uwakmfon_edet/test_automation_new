
module.exports = {
  'Login with InValid Creds' : function (browser) {
    browser
   .url('http://elearn.abudlc2.test.vggdev.com')
      .waitForElementVisible('body', 6000)
      .verify.elementPresent('#UserName')
      .pause(100)
      .setValue('input[id=UserName]', 'test31mba1111')
      .verify.elementPresent('#Password')
      .setValue('input[id=Password]', 'password1')
      .useXpath()
      .verify.elementPresent('//*[@id="content"]/div/div/div/form/div/button')
      .pause(500)
      .click('//*[@id="content"]/div/div/div/form/div/button')
      .verify.title('ELEARN.ABUDLC2.TEST.VGGDEV.COM - Login to continue - Vigilearn')  
      .pause(500)
      .useCss()
      .verify.elementPresent('.text-danger')
      .pause(900)
      .expect.element('.text-danger').text.to.contain("Error logging in")
      browser.pause(800)
       browser.verify.containsText('.text-danger', 'Error logging in. Please check username and password and try again.')
      browser.end()

  }
}
