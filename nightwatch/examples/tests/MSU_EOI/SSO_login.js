module.exports = {
  'SSO Login' : function (browser) {
    browser
        .url('https://register-test.agriyouthlab.com/')
        .waitForElementVisible('body', 6000)
        .verify.elementPresent('#idIHave')
        .click('#idIHave')
        .pause(1000)
        .verify.urlContains('https://sso-test.agriyouthlab.com/')
        .useXpath()
        .verify.elementPresent('//*[@id="loginform"]/div[1]/div/input') 
        .setValue('//*[@id="loginform"]/div[1]/div/input', 'bukola')
        .verify.elementPresent('//*[@id="loginform"]/div[2]/div/input') 
        .setValue('//*[@id="loginform"]/div[2]/div/input', 'password1')
        .verify.elementPresent('//*[@id="loginform"]/div[4]/div/button') 
        .click('//*[@id="loginform"]/div[4]/div/button')
        .end()

  }
}
