
module.exports = {
  'Login with Valid Credentials' : function (browser) {
    browser
     .url('http://vigicode.edutech.test.vggdev.com/Admin/index#/login')
      .waitForElementVisible('body', 6000)
      .verify.title(' Admin Setup')
      .verify.elementPresent('.text-center')
     // .expect.element('.text-center').text.to.contain('Please login to Access your Account')
      .pause(100)
      .verify.elementPresent('#signupInputEmail1')
      //.pause(100)
      .setValue('#signupInputEmail1', 'admin@abudlc.edu.ng')
      .verify.elementPresent('#signupInputPassword')
      .setValue('#signupInputPassword', 'Ca3thm31fy0ucan!')
      .useXpath()
      .verify.elementPresent('//*[@id="page-content"]/div/div/div/div/div[2]/form/div[2]/button')
      .pause(500)
      .click('//*[@id="page-content"]/div/div/div/div/div[2]/form/div[2]/button')
      .verify.urlContains('http://vigicode.edutech.test.vggdev.com/Admin/index#/')
      .verify.title(' Admin Setup')  
      .end()

  }
}
