module.exports = {
  'View Report with valid Elligibility status' : function (browser) {
    browser
    .url('https://register-test.agriyouthlab.com')
      .waitForElementVisible('body', 6000)
      .useXpath()
      .verify.elementPresent('//*[@id="idIHave"]')
      .click('//*[@id="idIHave"]')
      .pause(1000)
      
      // SSO Login
      //  .waitForElementVisible('body', 60000)
        .verify.urlContains('https://sso-test.agriyouthlab.com/')
        .pause(1000)
        .useXpath()
        .verify.elementPresent('//*[@id="loginform"]/div[1]/div/input') 
        .setValue('//*[@id="loginform"]/div[1]/div/input', 'bukola')
        .verify.elementPresent('//*[@id="loginform"]/div[2]/div/input') 
        .setValue('//*[@id="loginform"]/div[2]/div/input', 'password1')
        .verify.elementPresent('//*[@id="loginform"]/div[4]/div/button') 
        .click('//*[@id="loginform"]/div[4]/div/button')
        // End of SSO Login
      
      //verify landing page
      .verify.urlEquals('https://register-test.agriyouthlab.com/Admin/Dashboard')
      
      // Report page functionalities
        .useXpath()
        .verify.elementPresent('//*[@id="main-menu"]/li[1]/a/span')
        .click('//*[@id="main-menu"]/li[1]/a/span')
        .pause(1000)
        .verify.elementPresent('//*[@id="main-menu"]/li[1]/ul/li/a')
        .click('//*[@id="main-menu"]/li[1]/ul/li/a')
        .verify.title('Expression of Interest')
        .pause(1000)
        .useCss()
        .verify.elementPresent('#eligibility')       
        .click('#eligibility')   
        .useXpath()
        .verify.elementPresent('//*[@id="eligibility"]/option[2]')
        .click('//*[@id="eligibility"]/option[2]')
        .pause(1000)
        .useCss()
        .verify.elementPresent('#btnSubmit')
        .click('#btnSubmit')
        .pause(1000)
        .useXpath()
        .verify.eementPresent('//*[@id="EOIGrid"]/div[2]/span')
        .verify.element('//*[@id="EOIGrid"]/div[2]/span').text.to.contain('1-')
        
        
.end()

  }
}
