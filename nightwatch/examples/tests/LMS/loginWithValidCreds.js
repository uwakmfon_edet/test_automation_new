/**
 * Sample automated test scenario for Nightwatch.js
 *
 * > it navigates to google.com and searches for nightwatch,
 *   verifying if the term 'The Night Watch' exists in the search results
 */

module.exports = {
  'Login with Valid Creds' : function (browser) {
    browser
   .url('http://elearn.abudlc2.test.vggdev.com')
      .waitForElementVisible('body', 6000)
      .verify.elementPresent('#UserName')
      .pause(100)
      .setValue('input[id=UserName]', 'test31mba')
      .verify.elementPresent('#Password')
      .setValue('input[id=Password]', 'password1')
      .useXpath()
      .verify.elementPresent('//*[@id="content"]/div/div/div/form/div/button')
      .pause(100)
      .click('//*[@id="content"]/div/div/div/form/div/button')
      .verify.title('Dashboard - Vigilearn')   
      .end()

  }
}
