
module.exports = {
  'SSO Test case' : function (browser) {
    browser
      .url('http://id.edutechsso.test.vggdev.com/login')
     .verify.title('Edutech NG')
     .waitForElementVisible('body', 1000)
     .useXpath()
     .click('//*[@id="wrapper"]/div/div/div/div[3]/div/p[2]/a/strong')
     .pause(1000)
     .click('//*[@id="to-recover"]')
     // Function to switch tabs
     // windows_handles() switches switches the focus from the active tab to a new tab / window
     .window_handles(function(result) {
                var newHandle = result.value[1];
                this.switchWindow(newHandle);
            })
     // Steps to execute on the new tab
     .verify.title('Edutech NG')
     .pause(1000)
     .verify.elementPresent('//*[@id="UserName"]')
     .setValue('//*[@id="UserName"]', 'aramide')
     .verify.elementPresent('//*[@id="wrapper"]/div/div/form/div[3]/div/input')
     .click('//*[@id="wrapper"]/div/div/form/div[3]/div/input')
     .verify.urlContains('Confirmation')
     .pause(1000)
     .verify.elementPresent('//*[@id="wrapper"]/div/div/hgroup/h1')
     .verify.containsText('//*[@id="wrapper"]/div/div/hgroup/h1','Confirmation.')
      .end()

  }
}
